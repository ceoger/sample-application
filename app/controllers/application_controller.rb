class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  include SessionsHelper

  def hello
    render json: { status: true , message: " I'm working "}
  end
end
