class UsersController < ApplicationController
  before_action :logged_in_user, only: [:edit, :update]

  def new
    @user = User.new
    # (params[:user])
  end

  # shows user record
  def show
    @user = User.find(params[:id])
  end


# create a new user
  def create
    @user = User.new(user_params)
    if @user.save
      flash[:success] = "Welcome to our app"
      redirect_to @user
      # handle a successful save
    else
      render 'new'
    end
  end

  def edit
    @user = User.find(params[:id])

  end


  private

    def user_params
      params.require(:user).permit(:name, :email, :password, :password_confirmation)
    end
end
